$(document).ready(function() {
    $("#btn-add").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/',
            data: {
                name: $("#task").val(),
                project_id: $("#project_id").val(),
            },
            dataType: 'json',
            success: function(data) {
                $('#frmAddTask').trigger("reset");
                $("#frmAddTask .close").click();
                window.location.reload();
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#add-task-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#add-task-errors').append('<li>' + value + '</li>');
                });
                $("#add-error-bag").show();
            }
        });
    });

    $("#btn-add-proj").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/project',
            data: {
                name: $("#project").val(),
                user_id: $("#user_id").val(),
            },
            dataType: 'json',
            success: function(data) {
                $('#frmAddTask').trigger("reset");
                $("#frmAddTask .close").click();
                window.location.reload();
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#add-project-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#add-project-errors').append('<li>' + value + '</li>');
                });
                $("#add-project-error-bag").show();
            }
        });
    });

    $("#btn-edit").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        console.log($("#task_edit").val());

        $.ajax({
            type: 'PUT',
            url: '/' + $("#task_id").val(),
            data: {
                name: $("#task_edit").val(),
                status: $("#status_edit").prop('checked'),
            },
            dataType: 'json',
            success: function(data) {
                $('#frmEditTask').trigger("reset");
                $("#frmEditTask .close").click();
                window.location.reload();
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#edit-task-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#edit-task-errors').append('<li>' + value + '</li>');
                });
                $("#edit-error-bag").show();
                console.log();
            }
        });
    });
    $("#btn-delete").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/' + $("#frmDeleteTask input[name=task_id]").val(),
            dataType: 'json',
            success: function(data) {
                $("#frmDeleteTask .close").click();
                window.location.reload();
            },
            error: function(data) {
                console.log(data);
            }
        });
    });

    $("#btn-delete-project").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/project/' + $("#frmDeleteProject input[name=project_id]").val(),
            dataType: 'json',
            success: function(data) {
                $("#frmDeleteProject .close").click();
                window.location.reload();
            },
            error: function(data) {
                console.log(data);
            }
        });
    });

    // $(".validate").validate({
    //     rules: {
    //         name: {
    //             required: true,
    //             email: true
    //         }
    //     },
    //     messages: {
    //         name: "Hello from validator",
    //     }
    // });
});

function addTaskForm(id) {
    $("#add-error-bag").hide();
    $('#addTaskModal').modal('show');
    $("#project_id").val(id);
}

function addProjectForm() {
    $("#add-project-error-bag").hide();
    $('#addProjectModal').modal('show');
}

function editTaskForm(task_id) {
    $.ajax({
        type: 'GET',
        url: '/' + task_id,
        success: function(data) {
            $("#edit-error-bag").hide();
            $("#frmEditTask input[name=name]").val(data.task.name);
            $("#frmEditTask input[name=task_id]").val(data.task.id);
            $('#editTaskModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}

function deleteTaskForm(task_id) {
    $.ajax({
        type: 'GET',
        url: '/' + task_id,
        success: function(data) {
            $("#frmDeleteTask #delete-title").html("Delete Task (" + data.task.name + ")?");
            $("#frmDeleteTask input[name=task_id]").val(data.task.id);
            $('#deleteTaskModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}

function deleteProjectForm(project_id) {
    $.ajax({
        type: 'GET',
        url: '/project/' + project_id,
        success: function(data) {
            $("#frmDeleteProject #delete-title").html("Delete Project (" + data.project.name + ")?");
            $("#frmDeleteProject input[name=project_id]").val(data.project.id);
            $('#deleteProjectModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}

