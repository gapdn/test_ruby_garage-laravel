<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $projects = Project::with('tasks')->where('user_id', '=', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('home', [
            //'tasks' => $tasks,
            'projects' => $projects,
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->input(), array(
            'name' => 'required|min:3|max:100',
            'project_id' => 'required',
        ));

        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ], 422);
        }

        $task = Task::create($request->all());

        return response()->json([
            'error' => false,
            'task'  => $task,
        ], 200);
    }

    public function storeUser(Request $request)
    {
        $validator = Validator::make($request->input(), array(
            'name' => 'required|min:3|max:100',
            'user_id' => 'required',
        ));

        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ], 422);
        }

        $project = Project::create($request->all());

        return response()->json([
            'error' => false,
            'project'  => $project,
        ], 200);
    }

    public function show($id)
    {
        $task = Task::find($id);

        return response()->json([
            'error' => false,
            'task'  => $task,
        ], 200);
    }

    public function showProject($id)
    {
        $project = Project::find($id);

        return response()->json([
            'error' => false,
            'project'  => $project,
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->input(), array(
            'name' => 'required|min:3|max:100',
        ));

        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ], 422);
        }

        $status = ($request->input('status') == 'true') ? 1 : 0;

        $task = Task::find($id);
        $task->name = $request->input('name');
        $task->status = $status;
        $task->save();

        return response()->json([
            'error' => false,
            'task'  => $task,
        ], 200);
    }

    public function destroy($id)
    {
        $task = Task::destroy($id);

        return response()->json([
            'error' => false,
            'task'  => $task,
        ], 200);
    }

    public function destroyProject($id)
    {
        $project = Project::destroy($id);
        $tasks = Task::where('project_id', '=', $id)->get();

        foreach ($tasks as $task) {
            $task->delete();
        }

        return response()->json([
            'error' => false,
            'project'  => $project,
        ], 200);
    }
}
