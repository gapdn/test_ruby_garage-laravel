<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/{id}', 'HomeController@show')->name('show');
Route::get('/project/{id}', 'HomeController@showProject')->name('showProject');
Route::post('/', 'HomeController@store')->name('store');
Route::post('/project', 'HomeController@storeUser')->name('storeUser');
Route::put('/{id}', 'HomeController@update')->name('update');
Route::delete('/{id}', 'HomeController@destroy')->name('destroy');
Route::delete('/project/{id}', 'HomeController@destroyProject')->name('destroyProject');