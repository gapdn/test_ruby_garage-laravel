
## Task

#### Task manager

I'​m a person who passionate about my own productivity. I want to manage my tasks and projects more effectively. I need a simple tool that supports me in controlling my task-flow
Functional requirements

- I want to be able to create/​​update/​​delete projects 

- I want to be able to add tasks to my project

- I want to be able to update/​​delete tasks

- I want to be able to prioritize tasks into a project 

- I want to be able to choose deadline for my task 

- I want to be able to mark a task as 'done'

#### Technical requirements

1.​ It should be a WEB application 

2.​ For the client side must be used:
HTML, CSS (any libs as Twitter Boorstrap, Blueprint ...),
JavaScript (any libs as jQuery, Prototype ...)
 
3.​ForaserversideanylanguageasRuby,PHP,Python,
JavaScript, C#, Java ...

4.​ It should have a client side and server side validation 05.​It should look like on screens (see attached file “t​est-task-ruby-courses-view.png”)​.
   
#### Additional requirements

- It should work like one page WEB application and should use AJAX technology, load and submit data without reloading a page.

- It should have user authentication solution and a user should only have access to their own projects and tasks.

- It should have automated tests for the all functionality

#### SQL task

Given tables:

1.​ tasks (id, name, status, project_id)

2.​projects (id, name) 

#### Technical requirements

- get all statuses, not repeating, alphabetically ordered

- get the count of all tasks in each project, order by tasks count
descending

```mysql
SELECT p.id, p.name, COUNT(t.name) as tasks_count FROM projects as p 
LEFT JOIN tasks as t ON p.id = t.project_id 
GROUP BY p.id 
ORDER BY tasks_count DESC;
```
- get the count of all tasks in each project, order by projects names

```mysql

SELECT p.id, p.name, COUNT(t.name) as tasks_count FROM projects as p 
LEFT JOIN tasks as t ON p.id = t.project_id 
GROUP BY p.id 
ORDER BY p.name;
```

- get the tasks for all projects having the name beginning with
"N" letter

```mysql
SELECT * FROM tasks WHERE name LIKE "N%";
```
- get the list of all projects containing the 'a' letter in the middle of the name, and show the tasks count near each project. Mention
that there can exist projects without tasks and tasks with project_id = NULL
```mysql
SELECT p.id, p.name, COUNT(t.name) as tasks_count FROM projects as p 
LEFT JOIN tasks as t ON p.id = t.project_id 
GROUP BY p.id HAVING p.name LIKE "%a%"
ORDER BY tasks_count;
```

- get the list of tasks with duplicate names. Order alphabetically

```mysql
SELECT id, name, COUNT(name) as count_name FROM tasks 
GROUP BY name 
HAVING count_name > 1;
```

- get list of tasks having several exact matches of both name and status, from the project 'Garage'. Order by matches count

```mysql
SELECT t.id, t.name, COUNT(t.name) as count_matches FROM tasks as t 
LEFT JOIN projects as p ON t.project_id = p.id 
WHERE p.name = "Garage"
GROUP BY t.name, t.status 
HAVING count_matches > 1
ORDER BY count_matches;
```

- get the list of project names having more than 10 tasks in status 'completed'. Order by project_id

```mysql
SELECT p.id, p.name, COUNT(t.id) as count_tasks FROM projects as p 
LEFT JOIN tasks as t ON p.id = t.project_id 
WHERE t.status = 1 
GROUP BY p.id 
HAVING count_tasks > 10
ORDER By p.id;
```

## Instalation

#### Used technologies

```
nginx/1.14.0

PHP 7.2.19

10.1.29-MariaDB-6

jquery/3.4.1

bootstrap/3.3.7
```

- Run commands

```
1. git clone https://gapdn@bitbucket.org/gapdn/test_ruby_garage-laravel.git
2. composer install
3. cp .env.example .env
4. php artisan key:generate
5. php artisan migrate
```

- Update '.env' file with your DB settings
```
...
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_db_name
DB_USERNAME=your_user_name
DB_PASSWORD=your_user_password
...
```
- Configure Nginx settings

- Enjoy ))