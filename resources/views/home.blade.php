@extends('layouts.app')
@php
//dd($projects);
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a onclick="event.preventDefault();addProjectForm();" class="btn btn-outline-success btn-block">Add new project</a>
        </div>

        @forelse($projects as $project)
            <div class="col-md-8">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-5">
                                <h2>{{ $project->name }}</h2>
                            </div>
                            <div class="col-sm-4">
                                <a onclick="event.preventDefault();addTaskForm({{ $project->id }});" href="#" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Task</span></a>
                            </div>
                            <div class="col-sm-3">
                                <a onclick="event.preventDefault();deleteProjectForm({{ $project->id }});" href="#" class="btn btn-outline-danger" data-toggle="modal"><i class="material-icons">&#xE872;</i><span>Delete Project</span></a>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Task</th>
                            <th>Created At</th>
                            <th>Done</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($project->tasks as $task)
                            <tr>
                                <td>{{$task->id}}</td>
                                <td>{{$task->name}}</td>
                                <td>{{$task->created_at}}</td>
                                <td>{{($task->status) ? 'Yes' : 'No'}}</td>
                                <td>
                                    <a onclick="event.preventDefault();editTaskForm({{$task->id}});" href="#" class="edit open-modal" data-toggle="modal" value="{{$task->id}}"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                    <a onclick="event.preventDefault();deleteTaskForm({{$task->id}});" href="#" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                </td>
                            </tr>
                        @empty
                           <!-- <div class="col-md-8">
                                Add your first task
                            </div>-->
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

        @empty
            <div class="col-md-8" style="text-align: center">
                <hr>
                There are no projects yet!
            </div>
        @endforelse
    </div>
</div>
@endsection
