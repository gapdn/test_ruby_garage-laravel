<!-- Add Task Modal Form HTML -->
<div class="modal fade" id="addProjectModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="validate" id="frmAddProject">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Add New Project
                    </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="add-project-error-bag">
                        <ul id="add-project-errors">
                        </ul>
                    </div>

                    <div class="form-group">
                        <label>
                            Project
                        </label>
                        <input class="form-control" id="project" name="project" required="" type="text" />
                    </div>
                </div>
                <div class="modal-footer">
                    @if( Auth::user() )

                        <input id="user_id" name="user_id" type="hidden" value="{{ Auth::user()->id }}"/>

                    @endif
                    <input class="btn btn-default" data-dismiss="modal" type="button" value="Cancel" />
                    <button class="btn btn-info" id="btn-add-proj" type="button" value="add">
                        Add New Project
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>