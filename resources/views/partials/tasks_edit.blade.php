<!-- Edit Modal HTML -->
<div class="modal fade" id="editTaskModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="validate" id="frmEditTask">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Edit Task
                    </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-task-errors">
                        </ul>
                    </div>
                    <div class="form-group">
                        <label>
                            Task
                        </label>
                        <input class="form-control" id="task_edit" name="name" required="" type="text"/>

                        @if( !empty($task) )

                            <input id="status_edit" type="checkbox" name="status" value="{{ $task->status }}">

                        @endif

                        <label for="status_edit">Done</label>
                    </div>
                </div>
                <div class="modal-footer">

                    @if( !empty($task) )

                        <input id="task_id" name="task_id" type="hidden" value="{{ $task->id }}"/>

                    @endif

                    <input class="btn btn-default" data-dismiss="modal" type="button" value="Cancel"/>
                    <button class="btn btn-info" id="btn-edit" type="button" value="add">
                        Update Task
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
