<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Task;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->boolean('status')->default(0);
            $table->bigInteger('project_id');
            $table->timestamps();
        });

        Task::create([
            'name' => 'Weekend hookup',
            'status' => false,
            'project_id' => 1,
        ]);

        Task::create([
            'name' => 'Late night coding',
            'status' => false,
            'project_id' => 1,
        ]);

        Task::create([
            'name' => 'Drink bear after coding',
            'status' => false,
            'project_id' => 2,
        ]);

        Task::create([
            'name' => 'Sleep well',
            'status' => false,
            'project_id' => 2,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
